#!/bin/bash

pid=$(pgrep -x redshift)

echo $pid
if [ -z "$pid" ]; then
	redshift -O 4000K &
	notify-send "redshift on" "redshift set for 4000 K"
else
	kill -SIGTERM "$pid"
	notify-send "redshift off" "redshift disabled"
fi

