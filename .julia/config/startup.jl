# imyxh's startup file for Julia REPL
using Pkg

try
    using LinearAlgebra
    using OhMyREPL
    using PhysicalConstants.CODATA2018: c_0, G, k_B, m_e, m_n, m_p, m_u, N_A,
        e as q_e, ħ, ε_0, μ_0, σ as σ_SB
    using Unitful
    using Unitful.DefaultSymbols
    using UnitfulAstro: Msun, Rsun, Mearth, Rearth
catch e
    if e isa ArgumentError
        using Pkg
        println("installing packages ...")
        Pkg.add([
            "LinearAlgebra",
            "OhMyREPL",
            "PhysicalConstants", "Unitful", "UnitfulAstro",
        ])
        println("packages installed, please restart REPL!")
    end
end

# extra constants
#const AU = 149597828677m
#const Msun = 1.9885E30kg
#const Mearth = 5.9724E24kg

ln(x) = log(x)
lg(x) = log(10, x)
lb(x) = log(2, x)

# turn off bracket autocomplete because it's bad for slime
enable_autocomplete_brackets(false)

# show more digits with Measurements.jl
#Base.active_repl.options.iocontext[:error_digits] = 8

