#!/bin/sh

# Back up a list of explicitly installed packages.
pacman -Qent > $HOME/scripts/pacman_native.txt
pacman -Qm > $HOME/scripts/pacman_foreign.txt
pip freeze --user > $HOME/scripts/pip_freeze.txt
gem list > $HOME/scripts/gem_list.txt
npm list > $HOME/scripts/npm_list.txt

# save gsettings
dconf dump / > $HOME/scripts/dconf_dump.txt

# TODO: back up a bunch of system .conf files

