#!/bin/bash

# TODO: rewrite for adwaita
# a few things:
# 1. libadwaita apps use Gradience. i think this is gtk.css
# 2. regular gtk4 apps should also be gtk.css
# 3. gtk3 apps need to toggle gtk-application-prefer-dark-theme in settings.ini

light_theme="'Materia-compact'"
dark_theme="'Materia-dark-compact'"
light_icons="'Papirus'"
dark_icons="'Papirus-Dark'"

current_theme=$(gsettings get org.gnome.desktop.interface gtk-theme)

if [[ $current_theme = $light_theme ]]; then
	gsettings set org.gnome.desktop.interface gtk-theme $dark_theme
	notify-send "dark theme" "GTK dark theme enabled"
else
	gsettings set org.gnome.desktop.interface gtk-theme $light_theme
	notify-send "light theme" "GTK light theme enabled"
fi

