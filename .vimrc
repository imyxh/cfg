" load defaults
if filereadable("/usr/share/vim/vimfiles/archlinux.vim")
    source /usr/share/vim/vimfiles/archlinux.vim
endif

filetype plugin on
syntax on

"" set block cursor on start
autocmd VimEnter * silent !echo -ne "\e[2 q"
" set IBeam shape in insert mode, underline shape in replace mode and block shape after exiting insert mode
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

set mouse=a

set tabstop=8
set number
highlight LineNr ctermfg=grey

