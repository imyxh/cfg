#!/bin/sh

# lock gnome keyring by restarting the daemon
# (we need to shut down evolution too)
#evolution --force-shutdown
#gnome-keyring-daemon -r -d

if [ -e /tmp/imyxh/lockscreen.png ]; then
	rm /tmp/imyxh/lockscreen.png
fi

# switch the output name when necessary

grim -o DVI-I-1 /tmp/imyxh/lockscreen1.png
grim -o DP-1 /tmp/imyxh/lockscreen2.png
convert /tmp/imyxh/lockscreen1.png -scale 5% -colorspace Gray -blur 1 \
	-resize 2000% /tmp/imyxh/lockscreen1.png
convert /tmp/imyxh/lockscreen2.png -scale 5% -colorspace Gray -blur 1 \
	-resize 2000% /tmp/imyxh/lockscreen2.png

swaylock -fFklr -i DVI-I-1:/tmp/imyxh/lockscreen1.png \
	-i DP-1:/tmp/imyxh/lockscreen2.png \
	-c 000000 \
	--inside-color FFFFFF22 \
	--ring-color 11111122 \
	--key-hl-color FFFFFF \
	--layout-bg-color 00000099 \
	--inside-ver-color BD9AEF99 \
	--ring-ver-color BD9AEF99 \
	--inside-wrong-color F86D8B99 \
	--ring-wrong-color F86D8B99 \
	--inside-clear-color FFCC6699 \
	--ring-clear-color FFCC6699 \
	--font Iosevka

