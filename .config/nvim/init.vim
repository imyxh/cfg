" vim: set foldmethod=marker :

" VIM-PLUG {{{
" --------

" install vim-plug if missing
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

" Make sure you use single quotes

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'fxn/vim-monochrome'
Plug 'arcticicestudio/nord-vim'
Plug 'tpozzi/Sidonia'

Plug 'tpope/vim-fugitive'

Plug 'junegunn/goyo.vim'

Plug 'iamcco/markdown-preview.nvim', {
    \ 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']
\ }
Plug 'lervag/vimtex'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'jpalardy/vim-slime', {'branch': 'main'}
Plug 'goerz/jupytext.vim'
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }

Plug 'reedes/vim-pencil'

" coq (completion)
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}               " main
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}    " snippets
" lua & third party sources -- See https://github.com/ms-jpq/coq.thirdparty
" Need to **configure separately**
Plug 'ms-jpq/coq.thirdparty', {'branch': '3p'}
" - shell repl
" - nvim lua api
" - scientific calculator
" - comment banner
" - etc

call plug#end()
" }}}

" EDITING {{{
" -------

" load Arch's defaults
source /usr/share/vim/vimfiles/archlinux.vim

" set block cursor on start
autocmd! VimEnter * silent !echo -ne "\e[2 q"
" IBeam cursor in insert mode, block upon exiting, underline in replace
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" avoid the escape key (and ctrl-c)
inoremap jj <ESC>

" reduce timeout for commands like ] and j
set timeoutlen=200

" folds
set foldmethod=indent
set foldminlines=4
set foldnestmax=4

" comment continuation on enter key
set formatoptions+=cro

" only soft wrap at sensible locations
set linebreak

" read live changes to file on focus update
set autoread
" }}}

" FUNCTIONS {{{
" ---------

" get hex offset in file
function! FileOffset()
    return printf('0x%X', line2byte(line('.')) + col('.') - 2)
endfunction

" F2 to show syntax highlighting groups for word under cursor (dev tool)
nmap <F2> :call <SID>SynStack()<CR>
function! <SID>SynStack()
    if !exists("*synstack")
        return
    endif
    echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

" F3 to set nontext and whitespace settings
nmap <F3> :call <SID>HiInvisibles()<CR>
function! <SID>HiInvisibles()
    hi NonText ctermfg=DarkGray guifg=#444444
    hi Whitespace ctermfg=DarkGray guifg=#444444
endfunc

" F4 to rotate between themes
nmap <F4> :call <SID>RotateTheme()<CR>
function! <SID>RotateTheme()
    if !exists('g:rot_theme_index')
        let g:rot_theme_index = 0
    endif
    if !exists('g:rot_theme_list')
        let g:rot_theme_list = ['eta-carinae', 'etacar-rosemary', 'monochrome',
            \ 'nord', 'sidonia']
    endif
    execute 'colorscheme ' . g:rot_theme_list[g:rot_theme_index]
    call <SID>HiInvisibles()
    echo 'switched to theme ' . g:rot_theme_index
        \ . ': ' . g:rot_theme_list[g:rot_theme_index]
    let g:rot_theme_index = (g:rot_theme_index + 1) % len(g:rot_theme_list)
endfunc

" F5 to toggle xxd
nmap <F5> :call <SID>HexToggle()<CR>
function! <SID>HexToggle()
    if (&ft == 'xxd')
        %!xxd -r
        if exists('b:xxd_ft')
            let &ft = b:xxd_ft
        else
            set ft =
        endif
    else
        let b:xxd_ft = &ft
        %!xxd
        set ft = xxd
    endif
endfunc
" }}}

" THEMING {{{
" -------

set guifont=Iosevka:h10

let g:monochrome_italic_comments = 1

" see below where this is defined
call <SID>RotateTheme()

" terminal configurations
tnoremap <Esc> <C-\><C-n>
let g:terminal_color_0  = '#222222'  " black
let g:terminal_color_8  = '#666666'
let g:terminal_color_1  = '#F86D8B'  " red
let g:terminal_color_9  = '#FF4E4E'
let g:terminal_color_2  = '#59E13E'  " green
let g:terminal_color_10 = '#93EA5E'
let g:terminal_color_3  = '#FEA636'  " yellow
let g:terminal_color_11 = '#FFE863'
let g:terminal_color_4  = '#6486E4'  " blue
let g:terminal_color_12 = '#84AFE1'
let g:terminal_color_5  = '#BD9AEF'  " magenta
let g:terminal_color_13 = '#FC6FD0'
let g:terminal_color_6  = '#85C3C8'  " cyan
let g:terminal_color_14 = '#53ACBF'
let g:terminal_color_7  = '#DDDDDD'  " white
let g:terminal_color_15 = '#CCCCCC'
" }}}

" FILETYPES {{{
" ---------

filetype plugin on
filetype indent off
" default setting: c-like
set shiftwidth=0
set tabstop=8
set noexpandtab
set colorcolumn=80
set textwidth=80
set nospell

augroup configurefiletypes
    autocmd!
    autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
    autocmd BufRead,BufNewFile *.s,*.S set filetype=gas
    autocmd BufRead,BufNewFile *.sage,*.spyx,*.pyx set filetype=python
    " uhh so typing :make for any python ft will try to run sage on it :)
    autocmd FileType python set makeprg=sage\ %
    autocmd FileType html,xml setlocal ts=2 sw=2 et
    autocmd FileType conf,css,javascript,julia,markdown setlocal ts=4 sw=4 et
    autocmd FileType PKGBUILD,python,ruby,rust,scss,vim setlocal ts=4 sw=4 et
    autocmd FileType rust setlocal cc=100 tw=100
    autocmd FileType markdown,tex,text setlocal spell
    " disable airline's mixed-indent file checking in some filetypes
    autocmd FileType tex,text let b:airline_whitespace_checks =
        \ [ 'indent', 'trailing', 'long', 'conflicts' ]
    " disable airline's trailing space and mixed-indent checks in markdown
    autocmd FileType markdown let b:airline_whitespace_checks =
        \ [ 'indent', 'long', 'conflicts' ]
    autocmd FileType markdown syn match markdownError "\w\@<=\w\@="

    " markdown compilation with \ll (like vimtex)
    autocmd FileType markdown nmap <buffer> \ll :MarkdownPreviewToggle<CR>
    " python running
    autocmd FileType python nmap <buffer>
        \ \ll :w<CR>:exec '!python' shellescape(@%, 1)<CR>
augroup END

" hybrid line numbering
set number norelativenumber
augroup numbertoggle
    autocmd!
    autocmd InsertEnter * set relativenumber
    autocmd InsertLeave * set norelativenumber
augroup END
" }}}

" SYSTEM INTEGRATION {{{
" ------------------
" mouse
set mouse=a
" use system clipboard
set clipboard=unnamedplus
" :hardcopy settings
" TODO
" can't get postscript fonts working :(

" greek keyboard mapping
set langmap=ΑA,ΒB,ΨC,ΔD,ΕE,ΦF,ΓG,ΗH,ΙI,ΞJ,ΚK,ΛL,ΜM,ΝN,ΟO,ΠP,QQ,ΡR,ΣS,ΤT,ΘU,ΩV,
    \WW,ΧX,ΥY,ΖZ,αa,βb,ψc,δd,εe,φf,γg,ηh,ιi,ξj,κk,λl,μm,νn,οo,πp,qq,ρr,σs,τt,
    \θu,ωv,ςw,χx,υy,ζz
" }}}

" NEOVIDE {{{
" ------------------

let g:neovide_padding_top = 8
let g:neovide_padding_bottom = 8
let g:neovide_padding_right = 8
let g:neovide_padding_left = 8

let g:neovide_transparency = 0.9

let g:neovide_cursor_animation_length = 0.02

" }}}

" PLUGIN CONFIGURATION {{{
" --------------------

" vimtex
let g:vimtex_view_general_viewer = 'evince'
let g:vimtex_indent_enabled = 0
let g:vimtex_matchparen_enabled = 0
let g:vimtex_motion_matchparen = 0
let g:vimtex_compiler_method = 'latexmk'
"let g:vimtex_compiler_method = 'tectonic'
" tectonic is good but doesn't work with svg yet
" (https://github.com/tectonic-typesetting/tectonic/issues/38)
" enable shell escape for latex (i know, i know) so we can use fucking svgs
let g:vimtex_compiler_tectonic = {
    \ 'build_dir' : '',
    \ 'options' : [
    \   '--keep-logs',
    \   '--synctex',
    \   '-Z shell-escape'
    \ ],
    \}
let g:vimtex_compiler_latexmk = {
    \ 'build_dir' : '',
    \ 'callback' : 1,
    \ 'continuous' : 1,
    \ 'executable' : 'latexmk',
    \ 'hooks' : [],
    \ 'options' : [
    \   '-shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
let g:vimtex_compiler_latexrun = {
    \ 'build_dir' : '',
    \ 'options' : [
    \   '-shell-escape',
    \   '-verbose-cmds',
    \   '--latex-args="-synctex=1"',
    \ ],
    \}

" airline
let g:airline_powerline_fonts = 1
let g:airline_theme = 'minimalist'
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.dirty='*'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.notexists = '?'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline#extensions#whitespace#checks = [
    \ 'indent', 'trailing', 'long', 'mixed-indent-file', 'conflicts'
\ ]

" fix airline mix-indent-file error (add files here when encountered)
let airline#extensions#c_like_langs = [
    \ 'c', 'c.doxygen', 'cpp', 'go', 'javascript'
\ ]
let g:airline_section_z = airline#section#create_right(
    \ ['%l:%c, %p %% [%{FileOffset()}]']
\ )

" slime
let g:slime_target = 'neovim'

" set Goyo configuration
function! s:goyo_enter()
    set number
    set listchars=
    set nolist
    set mouse=a
endfunction
if !exists('*s:goyo_leave')
    function! s:goyo_leave()
        so $MYVIMRC
    endfunction
endif
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
" }}}

