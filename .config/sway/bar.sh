#!/bin/sh

# kill old processes
killall bar.sh -o 1s


while true; do

	temps=$(sensors -f | sed -n 's/ *+\([0-9]\)/\1/; s/Tctl:\([0-9]*\).*/\1 °F APU/p; s/Composite:\([0-9]*\).*/, \1 °F SSD/p' | tr -d '\n')

	# note that volume and mic are for left only, not average of both
#	vol="aux "
#	vol+="$(amixer sget Master | sed -nzE 's@.*\[([0-9]+)%.*@\1 %@p')"
#	mic="mic "
#	if [ "$(amixer sget Capture | grep off)" ]; then
#		mic+="off"
#	else
#		mic+="$(amixer sget Capture | sed -nE 's/.*\[([0-9]+)%.*/\1 %/p; /%/q')"
#	fi
#
	memav=$(cat /proc/meminfo | head -n 3 | tail -n 1 | sed -r 's/[^0-9]//g')
	memav=$(bc <<< "scale=3; $memav / 1048576")
	memav+=" GiB RAM available"

	net="$(nmcli | grep -E 'enp.*: connected' | sed 's/: connected to/,/g')"
	if [ -z "$net" ]; then
		net=$(iwgetid | sed 's/    /, /')
	fi
	if [ "$(nmcli | grep VPN)" ]; then
		net="${net}, VPN"
	fi

	time=$(date +'%F, %A    <b>◔ %T</b> %Z     ')

#	echo "${temps}        ${vol}, ${mic}        ${memav}        ${net}        ${time}"

	echo "${temps}        ${memav}        ${net}        ${time}"
	sleep 0.5

done

