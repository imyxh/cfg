#!/usr/bin/env zsh
set -e

packages=(mingw-w64-binutils-bin mingw-w64-headers-bin mingw-w64-crt-bin \
	mingw-w64-winpthreads-bin mingw-w64-gcc-bin)

for pkg in $packages; do
	cd $pkg
	git pull
	makepkg -scif --skippgpcheck
	cd ..
done

