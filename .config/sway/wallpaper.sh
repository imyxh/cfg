#!/bin/bash

# kill old processes
killall wallpaper.sh -o 1s

images=()

IFS=$'\n'  # delimit by newline instead of space

from_home=$(find "${HOME}/Pictures/Wallpapers" -maxdepth 2 -regex ".*\.\(png\|jpg\|jpeg\|tiff\)")
for f in $from_home; do
	images+=("$f")
done

images=( $(shuf -e "${images[@]}") )

for f in "${images[@]}"; do
	echo $f
done

while true; do
	for image in "${images[@]}"; do
		echo "$image" > /tmp/imyxh/current-wallpaper.txt
		killall swaybg
		swaybg -i "$image" -m fill &
		sleep 5m
	done
done

