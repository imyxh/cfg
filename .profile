# fcitx
export XMODIFIERS='@im=fcitx'
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx

# theming
export GTK_THEME=$(gsettings get org.gnome.desktop.interface gtk-theme | tr -d \')

# add a few things to PATH; fix if any of these folder names get updated
PATH=$PATH:/usr/bin/core_perl
PATH=/opt/rocm/bin:$PATH
PATH=$PATH:$HOME/.local/bin
GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
PATH="$PATH:$GEM_HOME/bin"
export PATH

#export LD_LIBRARY_PATH=/opt/rocm/lib:/usr/lib/julia

if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
	# a lot of apps are broken by these changes; use ~/.local/bin/*-wl
	export GDK_BACKEND="wayland"
	export MOZ_ENABLE_WAYLAND=1
	export QT_QPA_PLATFORM="wayland"
	export QT_QPA_PLATFORMTHEME="qt5ct"
	export QT_AUTO_SCREEN_SCALE_FACTOR=0
	export _JAVA_AWT_WM_NONREPARENTING=1
	export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel"
	if ! [ -z "$SWAYSOCK" ]; then
		# sway-specific tweak
		export XDG_CURRENT_DESKTOP=sway
	fi
fi


# enable sketchy Blender Cycles OpenCL kernel
#export CYCLES_OPENCL_SPLIT_KERNEL_TEST=1

export GPG_TTY=$(tty)	# use tty for gpg? hopefully?

# julia hack (Makie.jl issue 1460) (also because they use old libs lol)
#alias julia='env LD_LIBRARY_PATH="$HOME/Downloads/julia-1.8.4/lib" __GLX_VENDOR_LIBRARY_NAME=mesa MESA_LOADER_DRIVER_OVERRIDE=zink GALLIUM_DRIVER=zink QT_QPA_PLATFORM=xcb julia'
alias julia='env LD_LIBRARY_PATH="$HOME/Downloads/julia-1.10.1/lib" julia'

